package com.instreamatic.adadapter.view.core;

import android.app.Activity;

public interface IAdAdapterViewBundleFactory {
    IAdAdapterViewBundle build(AdAdapterLayoutType var1, Activity var2);
}
