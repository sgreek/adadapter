package com.instreamatic.adadapter.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.VideoView;

import com.instreamatic.adadapter.AdAdapter;
import com.instreamatic.adadapter.TritonAdapterEvent;
import com.instreamatic.adadapter.view.core.AdAdapterLayoutType;
import com.instreamatic.adadapter.view.core.AdAdapterViewType;
import com.instreamatic.adadapter.view.core.IAdAdapterViewBundle;
import com.instreamatic.adadapter.view.core.IAdAdapterViewBundleFactory;
import com.instreamatic.adman.event.EventDispatcher;
import com.tritondigital.ads.Ad;
import com.tritondigital.ads.SyncBannerView;

import java.lang.ref.WeakReference;


/**
 * Shows how to display an on-demand ad.
 */
abstract class BaseAdsView implements
        MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {
    final private static String TAG = "AdsActivity";

    private AdAdapter adAdapter;
    private BaseAdsView.AdWrapEventListener adWrapEventListener = new BaseAdsView.AdWrapEventListener();

    protected WeakReference<Activity> contextRef;
    protected View view;
    protected ViewGroup target;
    private IAdAdapterViewBundle bundle;

    protected boolean displaying;

    private SyncBannerView mBannerView;
    private VideoView mVideoView;


//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(getLayout());
//    }

    //fromLayout(activity, R.layout.adman_portrait);

    public BaseAdsView(Activity context) {
        displaying = false;
        setActivity(context);
    }

    public void bindAd(AdAdapter adAdapter) {
        this.adAdapter = adAdapter;
        EventDispatcher dispatcher = adAdapter.getTritonAdapter().getDispatcher();
        dispatcher.addListener(TritonAdapterEvent.TYPE, adWrapEventListener);
    }

    public void unbindAd() {
        if (this.adAdapter == null) return;
        this.done();
        this.adAdapter = null;
    }

    public View getView() {
        return this.bundle != null ? this.bundle.get(AdAdapterViewType.CONTAINER_AD) : null;
    }

    public void setTarget(ViewGroup var1) {
        this.target = target;
    }

    public ViewGroup getTarget() {
        if (this.target != null) {
            return this.target;
        } else {
            Activity context = this.contextRef.get();
            if(context == null) {
                Log.w(TAG, "Activity is null");
                return null;
            }
            return (ViewGroup) context.getWindow().getDecorView().findViewById(android.R.id.content);
        }
    }

    public void show() {
        Activity context = contextRef.get();
        if (context == null) {
            Log.w(TAG, "Ad loading ERROR: NULL ad");
            return;
        }
        final Bundle ad = this.adAdapter != null ? this.adAdapter.getTritonAdapter().getCurrentAdBundle() : null;
        if ((ad == null) || ad.isEmpty()) {
            Log.w(TAG, "Ad loading ERROR: NULL ad");
            return;
        }
        context.runOnUiThread(new Runnable() {
            public void run() {
                prepareView();
                displaying = displayContent(ad);
                initVideoView();
                showAd(ad);
            }
        });

    }

    public void close() {
        Activity context = contextRef.get();
        if(context == null) return;

        if (displaying) {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    View viewContainer = getView();
                    if (viewContainer != null) {
                        getTarget().removeView(viewContainer);
                        displaying = false;
                        viewContainer.destroyDrawingCache();
                    }
                    else {
                        Log.e(TAG, "View layout not found");
                    }
                }
            });
        }
    }

    public void rebuild() {

    }

    abstract IAdAdapterViewBundleFactory factory();


    public void setActivity(Activity activity){
        WeakReference<Activity> weakRef = this.contextRef;
        this.contextRef = new WeakReference<>(activity);
        if (weakRef != null) {
            weakRef.clear();
        }
    }

    public void pause() {

    }

    public void resume() {

    }

    public void done() {
        if (this.adAdapter == null) return;
        EventDispatcher dispatcher = this.adAdapter.getTritonAdapter().getDispatcher();
        dispatcher.removeListener(TritonAdapterEvent.TYPE, adWrapEventListener);
        this.clearAd();
        this.close();
    }

    private IAdAdapterViewBundle buildViewBundle() {
        Activity context = contextRef.get();
        if (context == null) {
            Log.i(TAG, "Activity is null");
            return null;
        }

        int orientation = context.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            return factory().build(AdAdapterLayoutType.PORTRAIT, context);
        } else {
            return factory().build(AdAdapterLayoutType.LANDSCAPE, context);
        }
    }

    private void prepareView() {
        this.bundle = buildViewBundle();
        if (this.bundle == null){
            Log.e(TAG, "Layout has not built");
            return;
        }
        if (bundle.contains(AdAdapterViewType.BANNER_AD)) {
            bundle.get(AdAdapterViewType.BANNER_AD).setVisibility(View.INVISIBLE);
        }
        if (bundle.contains(AdAdapterViewType.VIDEO_AD)) {
            bundle.get(AdAdapterViewType.VIDEO_AD).setVisibility(View.INVISIBLE);
        }
    }

    private void displayBanner(final Bundle ad, Activity context) {
        mBannerView = (bundle != null && bundle.contains(AdAdapterViewType.BANNER_AD))
                ? bundle.get(AdAdapterViewType.BANNER_AD) : null;
        if (mBannerView == null) {
            return;
        }
        Point point = new Point();
        context.getWindowManager().getDefaultDisplay().getSize(point);
        point = mBannerView.getBestBannerSize(ad, point.x, point.y);
        if (point == null) {
            int w = 640;
            int h = 640;
            point = mBannerView.getBestBannerSize(ad, w, h);
        }
        if (point == null) {
            Log.w(TAG, "Banner size not found");
            return;
        }
        mBannerView.setBannerSize(point.x, point.y);
        mBannerView.setVisibility(View.VISIBLE);
    }

    protected boolean displayContent(final Bundle ad) {
        boolean isAdd = false;
        ViewGroup target = getTarget();
        if (target != null) {
            View viewContainer = getView();
            if (viewContainer != null) {
                target.addView(viewContainer, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                isAdd = true;
            }
            else{
                Log.e(TAG, "View container not found");
            }
        }
        Activity context = this.contextRef.get();
        if (ad != null && context != null) {
            displayBanner(ad, context);

        }
        return isAdd;
    }

    private void showAd(Bundle ad) {
        if (mBannerView != null) mBannerView.showAd(ad);

        String mimeType = ad.getString(Ad.MIME_TYPE);
        if (mimeType == null) {
            Log.d(TAG, "Warning: No audio/video");
        } else if (mimeType.startsWith("video")) {
            playVideoAd(ad);
        }
    }


    ///////////////////////////////////////////////////////////////////////////
    // Ads playback
    ///////////////////////////////////////////////////////////////////////////
    public void clearAd() {
        // Clear the previous banner content without destroying the view.
        if (mBannerView != null) {
            mBannerView.clearBanner();
            mBannerView = null;
        }
        // Cancel the video playback
        if (mVideoView != null) {
            mVideoView.stopPlayback();
            //mVideoView.setOnTouchListener(null);
            mVideoView.setVisibility(View.GONE);
            mVideoView = null;
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.d(TAG, "Playback completed.");
        clearAd();
        close();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.d(TAG, "Playback error: " + what + '/' + extra);
        clearAd();
        close();
        return true;
    }

    private void initVideoView() {
        mVideoView = (bundle != null && bundle.contains(AdAdapterViewType.VIDEO_AD))
                      ? bundle.get(AdAdapterViewType.VIDEO_AD) : null;
        if (mVideoView == null) {
            return;
        }
        mVideoView.setOnCompletionListener(this);
        mVideoView.setOnErrorListener(this);
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                // IMPORTANT: Track implressions
                mVideoView.start();
                Log.d(TAG, "Starting playback.");
            }
        });
    }


    @SuppressLint("ClickableViewAccessibility")
    private void playVideoAd(final Bundle ad) {
        try {
            Log.d(TAG, "Start video buffering");

            // Update the video view layout
            int width = ad.getInt(Ad.WIDTH);
            int height = ad.getInt(Ad.HEIGHT);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, height);
            mVideoView.setLayoutParams(layoutParams);
            mVideoView.setOnTouchListener(null);
            mVideoView.setVisibility(View.VISIBLE);

            // Handle the video clicks
            final String clickUrl = ad.getString(Ad.VIDEO_CLICK_THROUGH_URL);
            if (clickUrl != null) {
                mVideoView.setOnTouchListener(new View.OnTouchListener() {
                    // Not using onClickListener because of bugs in some OS versions.
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            Log.d(TAG, "Video clicked.");

                            // IMPORTANT: track video clicks
                            Ad.trackVideoClick(ad);

//!!!                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(clickUrl));
//!!!                            startActivity(intent);

                            // We can stop the video ad when the user has clicked it
                            clearAd();
                        }

                        return true;
                    }
                });
            }

            String mediaUrl = ad.getString(Ad.URL);
            mVideoView.setKeepScreenOn(true);
            mVideoView.setVideoURI(Uri.parse(mediaUrl));
            mVideoView.requestFocus();
        } catch (Exception e) {
            Log.d(TAG, "Video prepare exception: " + e);
        }
    }

    private class AdWrapEventListener implements TritonAdapterEvent.Listener {
        @Override
        public void onTritonAdapterEvent(TritonAdapterEvent admanEvent) {
            TritonAdapterEvent.Type type = admanEvent.getType();
            Log.d(TAG,"onWrapEvent, type: " + type);
            switch (type) {
                case AD_PLAYER_PLAYING:
                    show();
                    break;
                case AD_PLAYER_COMPLETE:
                case AD_PLAYER_FAILED:
                    close();
                    break;
            }
        }
    }
}
