package com.instreamatic.adadapter.view.core;

import android.app.Activity;

public abstract class AdAdapterViewBindFactory implements IAdAdapterViewBundleFactory {
    public AdAdapterViewBindFactory() {
    }

    protected abstract IAdAdapterViewBundle buildPortrait(Activity var1);

    protected abstract IAdAdapterViewBundle buildLandscape(Activity var1);


    public IAdAdapterViewBundle build(AdAdapterLayoutType type, Activity activity) {
        switch(type) {
            case PORTRAIT:
                return this.buildPortrait(activity);
            case LANDSCAPE:
                return this.buildLandscape(activity);
            default:
                return null;
        }
    }
}