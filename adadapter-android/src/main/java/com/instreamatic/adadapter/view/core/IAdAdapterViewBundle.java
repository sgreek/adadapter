package com.instreamatic.adadapter.view.core;

import android.view.View;


public interface IAdAdapterViewBundle {
    <T extends View> boolean contains(AdAdapterViewType<T> var1);

    <T extends View> T get(AdAdapterViewType<T> var1);

    AdAdapterViewBind getById(int var1);
}
