package com.instreamatic.adadapter;

import android.content.Context;
import android.util.Log;

import com.instreamatic.adman.AdmanRequest;
import com.instreamatic.adman.Region;
import com.instreamatic.adman.Type;
import com.instreamatic.adman.event.AdmanEvent;
import com.instreamatic.adman.event.EventDispatcher;

final public class AdAdapter {
    final private static String TAG = "AdAdapter";

    private EventDispatcher dispatcher;
    private TritonAdapter tritonAdapter;
    private AdmanAdapter admanAdapter;
    private AdmanRequest admanRequestDefault = new AdmanRequest.Builder()
            .setSiteId(1249)
            .setRegion(Region.GLOBAL)
            .setType(Type.VOICE)
            .build();

    private boolean startPlaying;
    private boolean autoSwitch = true;
    private AdType adType;
    private AdAdapter.AdmanEventListener admanEventListener = new AdAdapter.AdmanEventListener();
    private AdAdapter.TritonAdapterEventListener tritonAdapterEventListener = new AdAdapter.TritonAdapterEventListener();

    public AdAdapter(Context context) {
        this.startPlaying = false;
        this.adType = AdType.NONE;
        this.dispatcher = new EventDispatcher();
        tritonAdapter = new TritonAdapter(context);
        tritonAdapter.getDispatcher().addListener(TritonAdapterEvent.TYPE, tritonAdapterEventListener);

        admanAdapter = new AdmanAdapter(context, admanRequestDefault);
        admanAdapter.getDispatcher().addListener(AdmanEvent.TYPE, admanEventListener);
    }


    public void start() {
        Log.d(TAG, "start");
        this.load(adType, true);
    }

    public void preload(){
        Log.d(TAG, "preload");
        this.load(adType, false);
    }

    public void pause(){
        Log.d(TAG, "pause");
        if (tritonAdapter.isActive()) {
            tritonAdapter.pause();
        }
        if (admanAdapter.isActive()) {
            admanAdapter.pause();
        }
    }

    public void play() {
        Log.d(TAG, "play");
        if (admanAdapter.isActive() || admanAdapter.getCurrentAd() != null) {
            admanAdapter.play();
            return;
        }
        if (tritonAdapter.isActive() || tritonAdapter.getCurrentAdBundle() != null) {
            tritonAdapter.play();
        }
    }

    public void skip() {
        Log.d(TAG, "skip");
        tritonAdapter.skip();
        admanAdapter.skip();
    }

    public void reset() {
        Log.d(TAG, "reset");
        tritonAdapter.reset();
        admanAdapter.reset();
    }

    public boolean isPlaying() {
        return tritonAdapter.isPlaying() || admanAdapter.isPlaying();
    }

    public boolean isActive() {
        boolean activeTriton = tritonAdapter.isActive();
        boolean activeAdman = admanAdapter.isActive();
        if (activeTriton) {
            Log.d(TAG,"TritonAdapter is active");
        }
        if (tritonAdapter.isActive()) {
            Log.d(TAG,"AdmanAdapter is active");
        }
        return  activeTriton || activeAdman;
    }

    public TritonAdapter getTritonAdapter() {
        return tritonAdapter;
    }

    public AdmanAdapter getAdmanAdapter() {
        return admanAdapter;
    }

    public EventDispatcher getDispatcher() {
        return dispatcher;
    }

    public void addListener(AdAdapterEvent.Listener listener) {
        dispatcher.addListener(AdAdapterEvent.TYPE, listener);
    }

    public void removeListener(AdAdapterEvent.Listener listener) {
        dispatcher.removeListener(AdAdapterEvent.TYPE, listener);
    }

    private void load(AdType type, boolean startPlaying) {
        if (this.isActive()) {
            return;
        }
        this.startPlaying = startPlaying;
        if (type == AdType.ADMAN) {
            admanAdapter.load(this.startPlaying);
        } else if (type == AdType.TRITON) {
            tritonAdapter.load(this.startPlaying);
        } else {
            admanAdapter.load(this.startPlaying);
        }
    }

    private void doNext(AdAdapterEvent.Type event, AdType adType) {
        if (!autoSwitch) return;
        if ((event == AdAdapterEvent.Type.NONE) || (event == AdAdapterEvent.Type.FAILED)) {
            if (adType == AdType.ADMAN) this.load(AdType.TRITON, this.startPlaying);
        }
    }

    private class TritonAdapterEventListener implements TritonAdapterEvent.Listener {
        @Override
        public void onTritonAdapterEvent(TritonAdapterEvent event){
            Log.d(TAG, "onTritonAdapterEvent");
            switch (event.getType()) {
                case AD_LOADED:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.PREPARE, TAG, AdType.TRITON));
                    break;
                case AD_NONE:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.NONE, TAG, AdType.TRITON));
                    break;
                case AD_PLAYER_PREPARE:
                    break;
                case AD_PLAYER_READY:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.READY, TAG, AdType.TRITON));
                    break;
                case AD_PLAYER_PLAYING:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.STARTED, TAG, AdType.TRITON));
                    break;
                case ERROR:
                case AD_LOADED_ERROR:
                case AD_PLAYER_FAILED:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.FAILED, TAG, AdType.TRITON, "onTritonAdapterEvent: "+event.getType()));
                    break;
                case AD_PLAYER_COMPLETE:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.COMPLETED, TAG, AdType.TRITON));
                    break;
                case SKIPPED:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.SKIPPED, TAG, AdType.ADMAN));
                    break;
            }
        }
    }

    private class AdmanEventListener implements AdmanEvent.Listener {
        @Override
        public void onAdmanEvent(AdmanEvent event) {
            Log.d(TAG, "onAdmanEvent");
            switch (event.getType()) {
                case PREPARE:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.PREPARE, TAG, AdType.ADMAN));
                    break;
                case NONE:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.NONE, TAG, AdType.ADMAN));
                    doNext(AdAdapterEvent.Type.NONE, AdType.ADMAN);
                    break;
                case FAILED:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.FAILED, TAG, AdType.ADMAN));
                    doNext(AdAdapterEvent.Type.FAILED, AdType.ADMAN);
                    break;
                case READY:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.READY, TAG, AdType.ADMAN));
                    break;
                case STARTED:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.STARTED, TAG, AdType.ADMAN));
                    break;
                case ALMOST_COMPLETE:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.ALMOST_COMPLETE, TAG, AdType.ADMAN));
                    break;
                case COMPLETED:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.COMPLETED, TAG, AdType.ADMAN));
                    break;
                case SKIPPED:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.SKIPPED, TAG, AdType.ADMAN));
                    break;
            }
        }
    }

}
