package com.instreamatic.adadapter;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.instreamatic.adman.event.EventDispatcher;
import com.instreamatic.player.AudioPlayer;
import com.instreamatic.vast.VASTPlayer;
import com.tritondigital.ads.Ad;
import com.tritondigital.ads.AdLoader;
import com.tritondigital.ads.AdRequestBuilder;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class TritonAdapter {
    final private static String TAG = "TritonAdapter";

    protected WeakReference<Context> contextRef;
    private EventDispatcher dispatcher;

    protected boolean startPlaying;
    protected boolean playing;
    protected boolean loading;

    //Ad Triton
    private AdLoaderListener tAdLoaderListener = new AdLoaderListener();
    private AudioPlayerListener tAudioPlayerListener = new AudioPlayerListener();
    private AdLoader tAdLoader;
    private AdRequestBuilder tAdRequestBuilder;
    private Bundle tAd;
    private AudioPlayer audioPlayer;

    public TritonAdapter(Context context) {
        this.contextRef = new WeakReference<>(context);
        this.dispatcher = new EventDispatcher();
        this.playing = false;
        this.startPlaying = false;
    }

    public void start() {
        load(true);
    }

    public void preload() {
        load(false);
    }

    public void load(boolean startPlaying) {
        if (this.isPlaying()){
            Log.w(TAG, "Ad is already playing! Please complete the playback then request the new ad.");
            return;
        }
        this.startPlaying = startPlaying;
        tAdLoader = new AdLoader();
        tAdLoader.setListener(tAdLoaderListener);
        loading = true;
        tAdLoader.load(tAdRequestBuilder);
    }

    public void pause() {
        if (audioPlayer != null) {
            audioPlayer.pause();
        }
    }

    public void play() {
        if (audioPlayer != null) {
            audioPlayer.resume();
        }
    }

    public void skip() {
        dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.SKIPPED, TAG));
        this.reset();
    }

    public void reset() {
        Log.d(TAG, "Reset");
        if (audioPlayer != null) {
            audioPlayer.dispose();
            audioPlayer = null;
        }
        if (tAdLoader != null) {
            tAdLoader.cancel();
            tAdLoader = null;
        }
        tAd = null;
        loading = false;
        playing = false;
    }

    public AdRequestBuilder getRequest() {
        return tAdRequestBuilder;
    }

    public AdRequestBuilder updateRequest(AdRequestBuilder adRequestBuilder) {
        Log.d(TAG, "Update ad request");
        tAdRequestBuilder = adRequestBuilder;
        return adRequestBuilder;
    }


    public boolean isPlaying() {
        return this.playing;
    }


    public boolean isActive() {
        return this.isPlaying() || (this.getCurrentAdBundle() != null);
    }

    public Bundle getCurrentAdBundle() {
        return tAd;
    }

    public EventDispatcher getDispatcher() {
        return dispatcher;
    }

    private void startAd(Bundle ad) {
        List<TritonAdapterEvent> dispatcherEvents = new ArrayList<>();
        if ((ad == null) || ad.isEmpty()) {
            dispatcherEvents.add(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_NONE, TAG, "Ad loading EMPTY"));
        }
        Context context = contextRef.get();
        if (context == null) {
            dispatcherEvents.add(new TritonAdapterEvent(TritonAdapterEvent.Type.ERROR, TAG, "Context is null"));
        }
        if (dispatcherEvents.size() > 0) {
            loading = false;
            for (TritonAdapterEvent dispatcherEvent : dispatcherEvents) {
                dispatcher.dispatch(dispatcherEvent);
            }
            return;
        }

        tAd = ad;
        String mimeType = ad.getString(Ad.MIME_TYPE);
        Log.d(TAG, "Ad is mime_type: " + mimeType);
        if (mimeType != null && mimeType.startsWith("audio")) {
            startPlayer(context);
        } else {
            dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.ERROR, TAG, "Unsupported ad mime: " + mimeType));
        }
    }

    private void startPlayer(Context context) {
        String mediaUrl = tAd.getString(Ad.URL);
        audioPlayer = new AudioPlayer(context, mediaUrl, this.startPlaying);
        audioPlayer.setName("AdPlayerTriton");
        audioPlayer.setCompleteListener(tAudioPlayerListener);
        audioPlayer.setStateListener(tAudioPlayerListener);
        //audioPlayer.setProgressListener(tAudioPlayerListener);
    }

    private class AudioPlayerListener implements AudioPlayer.CompleteListener, AudioPlayer.ProgressListener, AudioPlayer.StateListener {
        public void onComplete() {
            Log.d(TAG, "AudioPlayer.onComplete");
        }

        public void onProgressChange(int position, int duration) {
            Log.d(TAG, "AudioPlayer.onProgressChange");
        }

        public void onStateChange(AudioPlayer.State state) {
            Log.d(TAG, "AudioPlayer.onStateChange, state: " + state);
            switch (state) {
                case PREPARE: //
                    dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_PLAYER_PREPARE, TAG));
                    break;
                case READY:
                    dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_PLAYER_READY, TAG));
                    break;
                case PLAYING:
                    if (playing) {
                        dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_PLAYER_PLAY, TAG));
                    } else {
                        playing = true;
                        loading = false;
                        dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_PLAYER_PLAYING, TAG));
                    }
                    break;
                case PAUSED:
                    dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_PLAYER_PAUSE, TAG));
                    break;
                case ERROR:
                    dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_PLAYER_FAILED, TAG));
                    reset();
                    break;
                case STOPPED:
                    dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_PLAYER_COMPLETE, TAG));
                    reset();
                    break;
            }
        }
    }

    private class AdLoaderListener implements AdLoader.AdLoaderListener {
        @Override
        public void onAdLoaded(AdLoader adLoader, Bundle ad) {
            Log.d(TAG, "AdLoader.onAdLoaded");
            dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_LOADED, TAG, ad));
            startAd(ad);
        }

        @Override
        public void onAdLoadingError(AdLoader adLoader, int errorCode) {
            Log.d(TAG, "AdLoader.onAdLoadingError");
            loading = false;
            dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_LOADED_ERROR, TAG, "Ad loading FAILED: " + AdLoader.debugErrorToStr(errorCode)));
            reset();
        }
    }
}
