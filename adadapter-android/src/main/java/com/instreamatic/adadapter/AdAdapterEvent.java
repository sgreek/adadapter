package com.instreamatic.adadapter;

import android.os.Bundle;

import com.instreamatic.adman.event.BaseEvent;
import com.instreamatic.adman.event.EventListener;
import com.instreamatic.adman.event.EventType;

public class AdAdapterEvent extends BaseEvent<AdAdapterEvent.Type, AdAdapterEvent.Listener> {

    final public static EventType<AdAdapterEvent.Type, AdAdapterEvent, Listener> TYPE = new EventType<AdAdapterEvent.Type, AdAdapterEvent, AdAdapterEvent.Listener>("AdAdapter") {
        @Override
        public void callListener(AdAdapterEvent event, AdAdapterEvent.Listener listener) {
            listener.onAdAdapterEvent(event);
        }
    };

    public enum Type {
        PREPARE,
        NONE,
        FAILED,
        READY,
        STARTED,
        ALMOST_COMPLETE,
        COMPLETED,
        SKIPPED,
    }
    final public AdType adType;
    final public String message;

    public AdAdapterEvent(AdAdapterEvent.Type type, String sender, AdType adType, String message) {
        super(type, sender);
        this.adType = adType;
        this.message = message;
    }

    public AdAdapterEvent(AdAdapterEvent.Type type, String sender, AdType adType) {
        this(type, sender, adType, null);
    }

    public AdAdapterEvent(AdAdapterEvent.Type type, String sender, String message) {
        this(type, sender, null, message);
    }

    public AdAdapterEvent(AdAdapterEvent.Type type, String sender) {
        this(type, sender, null, null);
    }

    @Override
    public EventType<AdAdapterEvent.Type, AdAdapterEvent, AdAdapterEvent.Listener> getEventType() {
        return TYPE;
    }

    public interface Listener extends EventListener {
        public void onAdAdapterEvent(AdAdapterEvent event);
    }
}
